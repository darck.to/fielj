$(document).ready(function() {

  $(document).on('focus',':input', function(){
    $(this).attr('autocomplete','off');
  })

  //carga files principal (buzon de entrada)
  carga_lista_users()
  function carga_lista_users(e) {
    $('#mainUsersBreadcrumb').html('Usuarios');
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('stat', 'lista');
    $.ajax({
      type: 'POST',
      url: 'adm/php/config/config-users.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content;
        $('#mainUsersList').html('')
        $.each(data, function (name, value) {
          if (value.success) {
            content = '<li class="list-odd">';
              content += '<i class="far fa-user fa-2x list-icon ma-7-t"></i>';
              content += '<div class="list-box">';
                content += '<p><b class="text-blue-grey">' + value.nombre + '</b>&nbsp;<i>' + value.type + '</i>&nbsp;|&nbsp;<small>' + value.mail + '</small></p>';
                content += '<small><span>' + value.telefono + '</span>&nbsp;|&nbsp;<span>' + value.celular + '</span></small>';
                content += '<p class="has-text-right">';
                  content += '<span class="handed editaUser" value=' + value.id + ' nom="' + value.nom + '" ape="' + value.ape + '" apm="' + value.apm + '" car="' + value.cargo + '" dir="' + value.direccion + '" tit="' + value.titulo + '" mai="' + value.mail + '" tel="' + value.telefono + '" cel="' + value.celular + '" typ=' + value.tipo + '><i class="far fa-edit font-one-3 has-text-info ma-half-lr"></i></span>';
                  content += '<span class="handed creaCertificado" value=' + value.id + ' mai="' + value.mail + '"><i class="fas fa-file-contract font-one-3 has-text-success ma-half-lr"></i></span>';
                content += '</p>';
              content += '</div>';
            content += '</li>';
            $('#mainUsersList').append(content)
          } else {
            toast(value.message)
          }
        })
        //creaCertificado
        $('.creaCertificado').on('click', function(e) {
          var id = $(this).attr('value');
          var mai = $(this).attr('mai');
          var content = '<div class="box has-background-white">';
            content += '<section class="section">';
              content += '<h1><b class="text-blue-grey text-u">Firma Electr&oacute;nica</b></h1>';
              content += '<hr/>';
              content += '<button class="button is-info ma-half-lr createCer" value="' + id + '">Generar</button>';
              content += '<a href="adm/assets/cert/' + id + '/fieljerez_' + mai + '.zip"><i class="fas fa-cloud-download-alt fa-2x is-pulled-right"></i></a>';
            content += '</section>';
          content += '</div>';
          modap(content);
          //crear certificado
          $('.createCer').on('click', function(e) {
            $(this).addClass('is-loading');
            crea_certificado(id, mai)
          })

          function crea_certificado(i,m) {
            var id = i;
            var mai = m;
            var stringaUth_key = localStorage.getItem("aUth_key");
            var stringaUth_user = localStorage.getItem("aUth_user");
            var form_data = new FormData();
            form_data.append('auth',stringaUth_key);
            form_data.append('user', stringaUth_user);
            form_data.append('id', id);
            form_data.append('mai', mai);
            $.ajax({
              type: 'POST',
              url: 'adm/php/cert/crea-cert.php',
              data: form_data,
              cache: false,
              contentType: false,
              processData: false,
              async: true,
              success: function(data) {
                $.each(data, function (name, value) {
                  if (value.success) {
                    xmodal();
                    $('.createCer').removeClass('is-loading');
                    carga_lista_users();
                    toast(value.message)
                  } else {
                    $('.createCer').removeClass('is-loading');
                    toast(value.message)
                  }
                })
              },
              error: function(xhr, tst, err) {
                toast(err)
              }
            })
          }
        })
        //edita Usuarios
        $('.editaUser').on('click', function(e) {
          var id = $(this).attr('value');
          var nom = $(this).attr('nom');
          var ape = $(this).attr('ape');
          var apm = $(this).attr('apm');
          var car = $(this).attr('car');
          var dir = $(this).attr('dir');
          var tit = $(this).attr('tit');
          var tel = $(this).attr('tel');
          var cel = $(this).attr('cel');
          var mai = $(this).attr('mai');
          var typ = $(this).attr('typ');
          var content = '<div class="box has-background-white">';
            content += '<section class="section">';
              content += '<h1><b class="text-blue-grey text-u">Edita el Usuario</b></h1>';
              content += '<hr/>';
              content += '<div class="field">';
                content += '<div class="control">';
                  content += '<input id="i_nom" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Nombre" value="' + nom + '">';
                  content += '<input id="i_ape" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Apellido Paterno" value="' + ape + '">';
                  content += '<input id="i_apm" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Apellido Materno" value="' + apm + '">';
                  content += '<input id="i_car" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Cargo" value="' + car + '">';
                  content += '<input id="i_dir" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Direcci&oacute;n" value="' + dir + '">';
                  content += '<input id="i_tit" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Titulo" value="' + tit + '">';
                  content += '<input id="i_tel" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Tel&eacute;fono" value="' + tel + '">';
                  content += '<input id="i_cel" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Celular" value="' + cel + '">';
                  content += '<input id="i_mai" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="@Mail" value="' + mai + '">';
                content += '</div>';
                content += '<div class="has-text-right">';
                  content += '<hr />';
                  if (typ != 0) {} else {
                    content += '<button class="button is-warning ma-half-lr activaUser" value="' + id + '">Activa</button>'
                  }
                  content += '<button class="button is-info ma-half-lr guardaUser" value="' + id + '">Guarda Cambios</button>';
                  content += '<hr />';
                  content += '<span class="handed borraUser" value="' + id + '"><i class="far fa-trash-alt fa-2x has-text-danger"></i></span>'
                content += '</div>';
              content += '</div>';
            content += '</section>';
          content += '</div>';
          modap(content);
          //guarda user
          $('.guardaUser').on('click', function(e){
            var id = $(this).attr('value');
            var stat = 'guarda';
            var nom = $('#i_nom').val();
            var ape = $('#i_ape').val();
            var apm = $('#i_apm').val();
            var car = $('#i_car').val();
            var dir = $('#i_dir').val();
            var tit = $('#i_tit').val();
            var tel = $('#i_tel').val();
            var cel = $('#i_cel').val();
            var mai = $('#i_mai').val();
            var form = {'nom':nom, 'ape':ape, 'apm':apm, 'car':car, 'dir':dir, 'tit':tit, 'tel':tel, 'cel':cel, 'mai':mai};
            modif_user(id, stat, form)
          })
          //activa user
          $('.activaUser').on('click', function(e){
            var id = $(this).attr('value');
            var stat = 'activa';
            modif_user(id, stat)
          })
          //borra user
          $('.borraUser').on('click', function(e){
            var id = $(this).attr('value');
            var stat = 'borra';
            modif_user(id, stat)
          })
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
    //modifica usuario
    function modif_user(e,f,g) {
      var id = e;
      var stat = f;
      if (stat == "guarda") {
        var form = g
      }
      var stringaUth_key = localStorage.getItem("aUth_key");
      var stringaUth_user = localStorage.getItem("aUth_user");
      var form_data = new FormData();
      form_data.append('auth',stringaUth_key);
      form_data.append('user', stringaUth_user);
      form_data.append('id', id);
      form_data.append('stat', stat);
      if (stat == "guarda") {
        $.each(form, function (key, value) {
          form_data.append(key, value);
        })
      }
      $.ajax({
        type: 'POST',
        url: 'adm/php/config/config-users.php',
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        async: true,
        success: function(data) {
          $.each(data, function (name, value) {
            if (value.success) {
              xmodal();
              carga_lista_users();
              toast(value.message)
            } else {
              toast(value.message)
            }
          })
        },
        error: function(xhr, tst, err) {
          toast(err)
        }
      })
    }
  }

});
