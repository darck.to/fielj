
<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../../functions/abre_conexion.php');
  include_once('../../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  $resultados = array();
  $log = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $id = mysqli_real_escape_string($mysqli,$_POST['id']);
  $mai = mysqli_real_escape_string($mysqli,$_POST['mai']);

  $sql =  $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $id_usr = $row['id_usr'];

    //consulta la direccion o jefatura del emisor del certificado
    $consulta =  $mysqli->query("SELECT dir FROM perf_table WHERE id_usr = '".$id_usr."'");
    if ($consulta->num_rows > 0) {
      $ren = $consulta->fetch_assoc();
      $dir = $ren['dir'];
    }

    //Create the certificate
    //The first step is to create a key pair which will be used to create a self-signed certificate.
    $configParams = array(
      'config' => 'C:\xampp\apache\conf\openssl.cnf',
      'digest_alg' => 'sha512',
      'private_key_bits' => 4096,                     //Bytes    512 1024 2048 4096, etc
      'private_key_type' => OPENSSL_KEYTYPE_RSA,     //Encryption type
    );
    $privkey = openssl_pkey_new($configParams);
    //$configParams on openssl_pkey_new is an optional argument.
    //Now, using the private key, we can create the certificate. First we define the certificate parameters:
    $data = array(
      "countryName" => "MX",
      "stateOrProvinceName" => "Zacatecas",
      "localityName" => "Jerez",
      "organizationName" => "Municipio de Jerez",
      "organizationalUnitName" => $dir,
      "commonName" => "Presidencia Municipal de Jerez",
      "emailAddress" => $mai
    );
    //And then we can create the certificate:
    $csr = openssl_csr_new($data, $privkey, $configParams);
    //Now we sign the certificate using the private key:
    $duration = 365;
    $sscert = openssl_csr_sign($csr, null, $privkey, $duration, $configParams);
    //Finally we can export the certificate and the private key:
    openssl_x509_export($sscert, $certout);
    $password = $mai;
    openssl_pkey_export($privkey, $pkout, $password, $configParams);
    //Note that a password is needed to export the private key. If a password is not needed, you must set $password to NULL (don't set it to empty string as the private key password will be an empty string).
    //create publick key
    $res = openssl_pkey_new($configParams);
    $pubKey = openssl_pkey_get_details($res);
    $pubKey = $pubKey['key'];
    //And save them
    $uploaddir = '../../assets/cert/' . $id . '/';
    if (!is_dir($uploaddir)) {
      //CREAMOS LA CARPETA DESTINO
      mkdir($uploaddir, 0755, true);
    }
    $one = file_put_contents($uploaddir . 'certificate.cer', $certout);
    $two = file_put_contents($uploaddir . 'privatekey.pem', $pkout);
    $tre = file_put_contents($uploaddir . 'publickey.pem', $pubKey);

    //ZipArchive
    $file1 = $uploaddir . 'certificate.cer';
    $file2 = $uploaddir . 'privatekey.pem';
    $file3 = $uploaddir . 'publickey.pem';
    $zip = new ZipArchive;
    if ($zip->open($uploaddir . 'fieljerez_' . $mai . '.zip', ZipArchive::CREATE) === TRUE) {
      // Add file to the zip file
      $zip->addFile($file1, 'certificate.cer');
      $zip->addFile($file2, 'privatekey.pem');
      $zip->addFile($file3, 'publickey.pem');

      // All files are added, so close the zip file.
      $zip->close();
    }
    //verificamos
    if (file_exists($uploaddir . 'fieljerez_' . $mai . '.zip')) {
      $resultados[] = array("success"=> true, "message"=> "Certificado creado");
      $log[] = array("success"=> true, "date"=> $fechaActual, "user"=>$auth, "message"=> "Certificado creado");
    } else {
      $resultados[] = array("success"=> false, "message"=> "Error en la creaci&oacute;n de Certificados");
      $log[] = array("success"=> false, "date"=> $fechaActual, "user"=>$auth, "message"=> "Error en la creaci&oacute;n de Certificados");
    }

  } else {
    $resultados[] = array("success"=> true, "message"=> "Auth Error");
    $log[] = array("success"=> false, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }


  //BITACORA
  $fileLog = '../../../data/assets/logs/upload.json';
  if (!file_exists($fileLog)) {
    $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileLog, 0777);
  }
  //nombre del archivo json y guardado
  $oldLog = file_get_contents($fileLog);
  $prelog = json_decode($oldLog, true);
  $prelog = array_merge($log, $prelog);
  //$prelog = $resultados;
  $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($prelog, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileLog, 0777);

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../../functions/cierra_conexion.php');
?>
