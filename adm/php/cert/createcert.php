
<?php
  //Create the certificate
  //The first step is to create a key pair which will be used to create a self-signed certificate.
  $configParams = array(
    'config' => 'C:\xampp\apache\conf\openssl.cnf',
    'digest_alg' => 'sha512',
    'private_key_bits' => 4096,                     //Bytes    512 1024 2048 4096, etc
    'private_key_type' => OPENSSL_KEYTYPE_RSA,     //Encryption type
  );
  $privkey = openssl_pkey_new($configParams);
  //$configParams on openssl_pkey_new is an optional argument.
  //Now, using the private key, we can create the certificate. First we define the certificate parameters:
  $data = array(
     "countryName" => "MX",
     "stateOrProvinceName" => "Zacatecas",
     "localityName" => "Jerez",
     "organizationName" => "Municipio de Jerez",
     "organizationalUnitName" => "Coordinación de la Unidad de Transparencia",
     "commonName" => "Presidencia Municipal de Jerez",
     "emailAddress" => "transparencia@jerez.gob.mx"
  );
  //And then we can create the certificate:
  $csr = openssl_csr_new($data, $privkey, $configParams);
  //Now we sign the certificate using the private key:
  $duration = 365;
  $sscert = openssl_csr_sign($csr, null, $privkey, $duration, $configParams);
  //Finally we can export the certificate and the private key:
  openssl_x509_export($sscert, $certout);
  $password = "fieljerezgobmx";
  openssl_pkey_export($privkey, $pkout, $password, $configParams);
  //Note that a password is needed to export the private key. If a password is not needed, you must set $password to NULL (don't set it to empty string as the private key password will be an empty string).
  //create publick key
  $res = openssl_pkey_new($configParams);
  $pubKey = openssl_pkey_get_details($res);
  $pubKey = $pubKey['key'];
  //And save them:
  file_put_contents("certificate.cer", $certout);
  file_put_contents("privatekey.pem", $pkout);
  file_put_contents('publickey.pem', $pubKey);
?>
