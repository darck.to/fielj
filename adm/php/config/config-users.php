<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../../functions/abre_conexion.php');
  include_once('../../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();
  $log = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $stat = mysqli_real_escape_string($mysqli,$_POST['stat']);

  if ($stat == "lista") { //carga listado para carga-area
    $consulta = "SELECT id_usr, type, nom, ape, apm, car, dir, tit, tel, cel, mai FROM perf_table";
  } elseif ($stat == "guarda") { //guarda cambios
    $id = mysqli_real_escape_string($mysqli,$_POST['id']);
    $nom = mysqli_real_escape_string($mysqli,$_POST['nom']);
    $ape = mysqli_real_escape_string($mysqli,$_POST['ape']);
    $apm = mysqli_real_escape_string($mysqli,$_POST['apm']);
    $car = mysqli_real_escape_string($mysqli,$_POST['car']);
    $dir = mysqli_real_escape_string($mysqli,$_POST['dir']);
    $tit = mysqli_real_escape_string($mysqli,$_POST['tit']);
    $tel = mysqli_real_escape_string($mysqli,$_POST['tel']);
    $cel = mysqli_real_escape_string($mysqli,$_POST['cel']);
    $mai = mysqli_real_escape_string($mysqli,$_POST['mai']);
    $consulta = "UPDATE perf_table SET nom = '".$nom."', ape = '".$ape."', apm = '".$apm."', car = '".$car."', dir = '".$dir."', tit = '".$tit."', tel = '".$tel."', cel = '".$cel."', mai = '".$mai."' WHERE id_usr = '".$id."'";
  } elseif ($stat == "activa") { //activa cuenta
    $id = mysqli_real_escape_string($mysqli,$_POST['id']);
    $consulta = "UPDATE perf_table SET type = 2 WHERE id_usr = '".$id."'";
  } elseif ($stat == "borra") { //borra cuenta
    $id = mysqli_real_escape_string($mysqli,$_POST['id']);
    $consulta = "DELETE FROM perf_table WHERE id_usr = '".$id."'";
    $consulta2 = "DELETE FROM auth_table WHERE id_usr = '".$id."'";
  }

  $sql =  $mysqli->query("SELECT nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();

    switch ($stat) {
      case "lista":
        $sql =  $mysqli->query($consulta);
        if ($sql->num_rows > 0) {
          while ($row = $sql->fetch_assoc()) {
            $type = "usuario";
            if ($row['type'] == 0) {$type = "nuevo";} elseif ($row['type'] == 1) {$type = "administrador";}
            $nombre = $row['nom'] . " " . $row['ape'] . " " . $row['apm'];
            $resultados[] = array("success"=> true, "id"=> $row['id_usr'], "nombre"=> $nombre, "nom"=> $row['nom'], "ape"=> $row['ape'], "apm"=> $row['apm'], "cargo"=> $row['car'], "direccion"=> $row['dir'], "titulo"=> $row['tit'], "telefono"=> $row['tel'], "celular"=> $row['cel'], "mail"=> $row['mai'], "type"=> $type, "tipo"=> $row['type']);
          }
          $log[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Error de consulta users");
        }
        break;

      case "guarda":
        $sql =  $mysqli->query($consulta);
        if ($sql) {
          $resultados[] = array("success"=> true, "message"=> "Usuario actualizado");
        } else {
          $log[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Error de guardado");
        }
        break;

      case "activa":
        $sql =  $mysqli->query($consulta);
        if ($sql) {
          $resultados[] = array("success"=> true, "message"=> "Usuario activado");
        } else {
          $log[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Error de actvaci&oacute;n");
        }
        //generamos la estructura de archivos que requiere
        $uploaddir = '../../../data/usr_assets/' . $id . '/';
        //CREAMOS LA CARPETA DESTINO
        mkdir($uploaddir, 0755, true);
        $fileTree = '../../../data/usr_assets/' . $id . '/' . $id . '_tree.json';
        $fileFinal = fopen($fileTree, 'w') or die ("error de lectura"); //archivo de feed
        fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
        fclose($fileFinal);
        chmod($fileTree, 0777);
        //agregamos el foliador
        $file = '../../../data/usr_assets/' . $id . '/folio.json';
        $filename = fopen($file, 'w') or die ("error de lectura");
  			$array[] = array("folio"=> 1);
        fwrite($filename, json_encode($array, JSON_PRETTY_PRINT));
        fclose($filename);
        chmod($file, 0777);
        break;

        case "borra":
          $sql =  $mysqli->query($consulta);
          $sql2 =  $mysqli->query($consulta2);
          if ($sql && $sql2) {
            $resultados[] = array("success"=> true, "message"=> "Usuario eliminado");
          } else {
            $log[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Error de eliminaci&oacute;n");
          }
          break;

    }

  } else {
    $log[] = array("success"=> false, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }

  //BITACORA
  $fileLog = '../../../data/assets/logs/upload.json';
  if (!file_exists($fileLog)) {
    $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileLog, 0777);
  }
  //nombre del archivo json y guardado
  $oldLog = file_get_contents($fileLog);
  $prelog = json_decode($oldLog, true);
  $prelog = array_merge($log, $prelog);
  //$prelog = $resultados;
  $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($prelog, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileLog, 0777);

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../../functions/cierra_conexion.php');
?>
