$(document).ready(function() {

  $(document).on('focus',':input', function(){
    $(this).attr('autocomplete','off');
  });

  carga_main_login();

});

function carga_main_login(e) {
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  comprueba_login(stringaUth_key,stringaUth_user);
  function comprueba_login(a,u) {
    $.ajax({
        type: "POST",
        url: "php/init/init-comprueba-auth.php",
        data: {
          auth : a,
          user : u
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $('.nav-container').load('templates/navigator/navigator.php');
          var aprobacion
          $.each(data, function (name, value) {
            aprobacion = value.success;
          });
          if (aprobacion == true) {
            $('.main-container').load('templates/main/main.html')
          } else {
            modal();
            $('.modal-content').load('templates/init/init-form.html')
          }
        },
        error: function(xhr, tst, err) {
          alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
          xmodal();
        },
        timeout: 10000
    })

  }
}
