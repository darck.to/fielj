// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.
$(document).ready(function() {
  //CARGA NAVIGATION
  window.navigation = function (x) {
    var nav = x;
    $('.main-container').load(nav)
  }
  //CREA UN TOAST MODAL
  window.toast = function(e) {
    if ($('#toast').length) {
      $('#toast').remove();
    }
    var toast = '<span id="toast" class="tag is-dark tag-toast">' + e + '</span>';
    $('body').append(toast);
    setTimeout(function(){
      if ($('#toast').length > 0) {
        $('#toast').fadeOut('fast', function() {
          $(this).remove()
        })
      }
    }, 4000)
  }
  //CREA UN MODAL PARA TODOS Y TODO
  window.modal = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
    modal += content;
    modal += '</div>';
    modal += '<div class="modal-absolute"></div>';
    modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    });
    $('.modal-background').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //MODAL SOLO CLAUSURABLE CON BOTON
  window.modap = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
    modal += content;
    modal += '</div>';
    modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //CIERRAMODAL
  window.xmodal = function(e) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
  }
  //MONEY FORMAT
  window.money_format = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  //CARGA MAIN FILES
  window.carga_main_files = function (x) {
    var folder = x
    $('.refresh-nav').attr('val',folder);
    localStorage.setItem('aUth_folder', folder);
    $('#mainFilesList').load('templates/main/main-files.html')
  }
})
