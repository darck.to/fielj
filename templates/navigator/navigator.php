<?php
  session_start();
?>
<div class="navbar-brand">
  <a class="navbar-item" href=".">
    <img src="img/logo.png">
  </a>

  <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
    <span aria-hidden="true"></span>
    <span aria-hidden="true"></span>
    <span aria-hidden="true"></span>
  </a>
</div>

<div class="navbar-menu">
  <div class="navbar-start">
    <a class="navbar-item" href=".">Inicio</a>

    <?php
      if (isset($_SESSION['auth'])) {
        echo"<div class=\"navbar-adm\"></div>";
        echo"<a class=\"navbar-item go-nav\" value=\"templates/main/main-doc.html\">Documentaci&oacute;n</a>
          <div class=\"navbar-item has-dropdown is-hoverable\">
            <a class=\"navbar-link\">M&aacute;s</a>
            <div class=\"navbar-dropdown\">
              <a id=\"session-close\" class=\"navbar-item\">Cierra Sesi&oacute;n</a>
              <hr class=\"navbar-divider\">
              <a class=\"navbar-item\">Reportar an problema</a>
            </div>
          </div>";
      } else {
        echo "<a class=\"navbar-item\" href=".">Inicia Sesi&oacute;n</a>";
      }
    ?>
  </div>

  <div class="navbar-end">
    <div class="navbar-item">
      <?php
        if (isset($_SESSION['auth'])) {
          echo"<i class=\"far fa-bell handed notiticationPopup\"></i>";
        } else {
          echo "<i class=\"far fa-bell-slash\"></i>";
        }
      ?>
    </div>
  </div>
</div>

<script type="text/javascript" src="scripts/navigator/navigator.js"></script>
