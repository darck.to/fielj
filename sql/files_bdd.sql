-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-08-2020 a las 22:38:14
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `files_bdd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_table`
--

CREATE TABLE `area_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `id_area` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del area o padre',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'nombre del area'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_table`
--

CREATE TABLE `auth_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `init_index` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'init index',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'usuario de login',
  `pas` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'password de usuario',
  `id_usr` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `auth_table`
--

INSERT INTO `auth_table` (`id`, `init_index`, `nom`, `pas`, `id_usr`) VALUES
(3, 's23ITnuJ', 'betote', '$2y$10$vyNLHbYUpZgqBReQRw5JfuunULPxHD4NkwnknvMR8fUE2kSmmeMnK', 'PPQedVKk'),
(4, 'B0H8HCxo', 'robertovaldez', '$2y$10$OGdR1XR3sZ.WO5RVzMUHa.3f78CjSKelnq91HLgc.0tG6l8BlsUP.', 'D4BOeOad'),
(5, 'brcyIW2r', 'prueba', '$2y$10$3vzcAWi/OdgUO9aX0znGzeIiTz.H.qlXdEW4AJiS048IoRUz21KYu', '1YKSKad7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perf_table`
--

CREATE TABLE `perf_table` (
  `id` int(11) NOT NULL COMMENT 'id de la table',
  `id_usr` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil',
  `id_fat` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del padre',
  `type` int(11) NOT NULL,
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ape` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apm` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `car` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'cargo del perfil',
  `dir` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'direccion o departamento',
  `tit` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'titulo del perfil',
  `tel` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cel` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `mai` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'mail del usr'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `perf_table`
--

INSERT INTO `perf_table` (`id`, `id_usr`, `id_fat`, `type`, `nom`, `ape`, `apm`, `car`, `dir`, `tit`, `tel`, `cel`, `mai`) VALUES
(3, 'PPQedVKk', '', 2, 'Beto asdfasdfasdf', 'Vasdfasdf', 'Guasdfa', 'dfasdf', 'fasdfasdf', 'asdfasdf', '4949494949', '9494949494', 'darck.to@gmail.com'),
(4, 'D4BOeOad', '', 1, 'Manuel Roberto', 'Valdez', 'Gutiérrez', 'Coordinador', 'Coordinación de la Unidad de Transparencia', 'C', '4941055849', '4941055849', 'valdezbeto@gmail.com'),
(5, '1YKSKad7', '', 2, 'Juan ', 'Test', 'Rodriguez', 'auxiliar', 'Transparencia', 'Sr.', '4941055555', '5558499856', 'test@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rela_table`
--

CREATE TABLE `rela_table` (
  `id` int(11) NOT NULL COMMENT 'id de la table',
  `remi` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'remitente',
  `dest` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'destinatario',
  `file` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'archivo',
  `fech` datetime NOT NULL COMMENT 'datetime'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `rela_table`
--

INSERT INTO `rela_table` (`id`, `remi`, `dest`, `file`, `fech`) VALUES
(1, 'D4BOeOad', 'PPQedVKk', 'oficio_D4BOeOad_D4BO_1.pdf', '2020-07-31 16:56:16'),
(2, 'D4BOeOad', 'PPQedVKk', 'oficio_D4BOeOad_31072020162417_fiej.pdf', '2020-07-31 17:24:16'),
(3, 'D4BOeOad', '1YKSKad7', 'oficio_D4BOeOad_31072020402517_fiej.pdf', '2020-07-31 17:25:40'),
(4, 'D4BOeOad', '1YKSKad7', 'oficio_D4BOeOad_31072020492617_fiej.pdf', '2020-07-31 17:26:49');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area_table`
--
ALTER TABLE `area_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `auth_table`
--
ALTER TABLE `auth_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perf_table`
--
ALTER TABLE `perf_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rela_table`
--
ALTER TABLE `rela_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `area_table`
--
ALTER TABLE `area_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla';

--
-- AUTO_INCREMENT de la tabla `auth_table`
--
ALTER TABLE `auth_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `perf_table`
--
ALTER TABLE `perf_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la table', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `rela_table`
--
ALTER TABLE `rela_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la table', AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
