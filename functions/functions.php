<?php

	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length) {
	    $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZasdfgqwertzxcvbpoiuylkjhmn';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	//FOLIOS ORDERS
	function leeFolio($id) {
		//NOMBRE DE ARCHIVO
		$file = $_SERVER['DOCUMENT_ROOT'] . '/fielj/data/usr_assets/' . $id . '/folio.json';
		if (file_exists($file)) {
			$filename = file_get_contents($file);
			$data = json_decode($filename, true);
			$folio = $data[0]['folio']; //este es el que se va
			//incrementa el numero de folio
			$viejo = $data[0]['folio'];
			$data[0]['folio'] = $viejo + 1;
			//LO VOLVEMOS A GUARDAR
			$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
			file_put_contents($file, $newJsonString);
			return $folio;
		} else { //si no existe
			$filename = fopen($file, 'w') or die ("error de lectura");
			$array[] = array("folio"=> 1);
      fwrite($filename, json_encode($array, JSON_PRETTY_PRINT));
      fclose($filename);
      chmod($file, 0777);
			$filename = file_get_contents($file);
			$data = json_decode($filename, true);
			$folio = $data[0]['folio']; //este es el que se va
			//lo incrementamos en 1
			$viejo = $data[0]['folio'];
			$data[0]['folio'] = $viejo + 1;
			//LO VOLVEMOS A GUARDAR
			$newJsonString = json_encode($data, JSON_PRETTY_PRINT);
			file_put_contents($file, $newJsonString);
			return $folio;
		}
	}

	//FUNCTION COMPRUEBA EMAIL
  function validaEmail($correo) {
	  if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
	    else return false;
	}

	//FUNCTION READ DOCS
	function read_docx($filename){
    $striped_content = '';
    $content = '';
    if(!$filename || !file_exists($filename)) return false;
    $zip = zip_open($filename);
    if (!$zip || is_numeric($zip)) return false;
    while ($zip_entry = zip_read($zip)) {
      if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
      if (zip_entry_name($zip_entry) != "word/document.xml") continue;
      $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
      zip_entry_close($zip_entry);
    }
    zip_close($zip);
    $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
    $content = str_replace('</w:r></w:p>', "\r\n", $content);
    $striped_content = strip_tags($content);

    return $striped_content;
	}

?>
