<?php
  function print_pdf($enombre, $ecargo, $edireccion, $etitulo, $nombre, $cargo, $direccion, $titulo, $texto, $emisor, $destinatario, $id) {

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $fechaymd = Date('dmYsiH');

    // Include the main TCPDF library (search for installation path).
    require_once('tcpdf/tcpdf.php');

    // Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends TCPDF {

      //Page header
      public function Header() {
        // Logo
        $image_file = '../../img/logo.jpg';
        $this->Image($image_file, 10, 10, 50, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 9);
        // Title
        $this->MultiCell(80, 10, $this->CustomHeaderText, 1, 'L', 0, 0, '120', '', true);
      }

      // Page footer
      public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'B', 8);
        // Page number
        $this->Cell(0, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
      }
    }

    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Municipio de Jerez');
    $pdf->SetTitle('Documento Certificado');

    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

    //header data
    $pdf->CustomHeaderText = 'DEPENDENCIA: ' . $direccion . ' | NÚMERO DE OFICIO: 12/2002 ' . 'EXPEDIENTE: 12/2020';
    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, 35, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // set font
    $pdf->SetFont('helvetica', 'B', 10);

    // add a page
    $pdf->AddPage();

    // set some text to print
    $pdf->SetFont('helvetica', 'N', 10);
    $txt = <<<EOD
    <div style="padding: 0, 50px;">
      <h4>$titulo $nombre</h4>
      <h4>$cargo, $direccion</h4>
      <H5>Presente:</H5>
      <p>$texto</p>
      <br><br>
      <h5 style="text-align: center; font-weight: bolder;">ATENTAMENTE</h5>
      <h5 style="text-align: center;">Jerez, Zac., a $fechaActual</h5>
      <h5 style="text-align: center; padding-top: 50px; font-weight: bolder;">$etitulo $enombre</h5>
      <h5 style="text-align: center; font-weight: bolder;">$ecargo $edireccion</h5>
    </div>
    EOD;

    $pdf->writeHTMLCell(150, 0, '30', '40', $txt, 0, 1, 0, true, '', true);

    // ---------------------------------------------------------

    //Close and output PDF document
    ob_clean();
    $pdf->Output($_SERVER['DOCUMENT_ROOT'] . 'fielj/data/usr_assets/' . $destinatario . '/oficio_' . $emisor . '_' . $fechaymd . '_fiej.pdf', 'F');
    return $_SERVER['DOCUMENT_ROOT'] . 'fielj/data/usr_assets/' . $destinatario . '/oficio_' . $emisor . '_' . $fechaymd . '_fiej.pdf';
    //============================================================+
    // END OF FILE
    //============================================================+
  }
?>
