<?php
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $sqlp = $mysqli->query("SELECT type FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqlp->num_rows > 0) {
      $rowp = $sqlp->fetch_assoc();
      $tipo = $rowp['type'];
      if ($tipo == 1) {
        $menu[] = array("main"=> "Configuraci&oacute;n", "level"=> "Usuarios", "url"=> "adm/templates/config/config-users.html");
        $resultados[] = array("success"=> true, "message"=> "Login Admin", "type"=> true, "menu"=> $menu);
      } else {
        $resultados[] = array("success"=> true, "message"=> "Login Usuario", "type"=> false);
      }
    }
  } else {
    $resultados[] = array("success"=> false, "message"=> "Error de login");
  }

	print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');

?>
