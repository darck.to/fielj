<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  if (empty($_POST['nom']) || empty($_POST['pas'])) {
    echo "El usuario o la contraseña no han sido ingresados correctamente!";
  } else {
    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $usuario_nombre = mysqli_real_escape_string($mysqli,$_POST['nom']);
    $usuario_clave = mysqli_real_escape_string($mysqli,$_POST['pas']);

    // comprobamos que los datos ingresados en el formulario coincidan con los de la BD
    $sqlogin = $mysqli->query("SELECT init_index, nom, pas FROM auth_table WHERE nom = '".$usuario_nombre."'");
    if ($sqlogin->num_rows > 0) {
      $row = $sqlogin->fetch_assoc();
      $validPassword = password_verify($usuario_clave, $row['pas']);
      if ($validPassword){
        $auth_nombre = $row["nom"];
        $auth_number = $row['init_index'];
        $resultados[] = array("success"=> true, "type"=>"login", "ip"=> $localIP, "date"=> $fechaActual, "aUth_key"=> $auth_number, "aUth_user"=> $auth_nombre);
      } else {
        $resultados[] = array("success"=> false, "type"=>"login", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, no login ");
      }
    } else {
      $resultados[] = array("success"=> false, "type"=>"login", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, contact support");
      //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
    }
  }

  //BITACORA
  $fileLog = '../../data/assets/logs/login.json';
  if (!file_exists($fileLog)) {
    $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileLog, 0777);
  }
  //nombre del archivo json y guardado
  $oldLog = file_get_contents($fileLog);
  $prelog = json_decode($oldLog, true);
  $prelog = array_merge($resultados, $prelog);
  //$prelog = $resultados;
  $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($prelog, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileLog, 0777);

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
