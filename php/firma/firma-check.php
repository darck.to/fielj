<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  $resultados = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql =  $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $id_usr = $row['id_usr'];

    //consulta la direccion o jefatura del emisor del certificado
    $consulta =  $mysqli->query("SELECT dir, mai FROM perf_table WHERE id_usr = '".$id_usr."'");
    if ($consulta->num_rows > 0) {
      $ren = $consulta->fetch_assoc();
      $dir = $ren['dir'];
      $mai = $ren['mai'];
    }

    //revisamos el archivo temp
    if (file_exists($_FILES['file']['tmp_name'])) {
      $zip = new ZipArchive;
      $zip->open($_FILES['file']['tmp_name']);
      $ssl = certificado_verifica($zip->getFromName('certificate.cer'));
      //revisa datos contra $data
      $ress = verifica_data($ssl, $dir, $mai);
      if ($ress) { //revisa la verificacion de certificado
        $resultados[] = array("success"=> true, "resultado"=> $ress,  "message"=> "Certificado valido");
      } else {
        $resultados[] = array("success"=> false, "resultado"=> false,  "message"=> "Certificado invalido");
      }
      $zip->close();
    }

  } else {
    $resultados[] = array("success"=> false, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }

  //verifica el certificado
  function certificado_verifica($cert) {
    //$fp = fopen($cert, "r");
    //$cert = fread($fp, 8192);
    //fclose($fp);
    return array(openssl_x509_parse($cert));
  }

  //verifica data
  function verifica_data($cert, $dir, $mai) {
    $fechaActual = Date('Y-m-d H:i:s');
    $data = array( //datos para cotejar
      "countryName" => "MX",
      "stateOrProvinceName" => "Zacatecas",
      "localityName" => "Jerez",
      "organizationName" => "Municipio de Jerez",
      "organizationalUnitName" => $dir,
      "commonName" => "Presidencia Municipal de Jerez",
      "emailAddress" => $mai
    );
    foreach ($cert as $content) { //lee certificado
      $ssl = array(
        "countryName" => $content['issuer']['C'],
        "stateOrProvinceName" => $content['issuer']['ST'],
        "localityName" => $content['issuer']['L'],
        "organizationName" => $content['issuer']['O'],
        "organizationalUnitName" => $content['issuer']['OU'],
        "commonName" => $content['issuer']['CN'],
        "emailAddress" => $content['issuer']['emailAddress'],
      );
      $arrays = ($data === $ssl);
      $from = date('Y-m-d H:i:s', $content['validFrom_time_t']);//desde
      $to = date('Y-m-d H:i:s', $content['validTo_time_t']);//hasta
      $vigencia = false;
      if ($fechaActual < $to) { //compara vigencia del certificado
        $vigencia = true;
      }
      if ($arrays and $vigencia) { //solo si las dos condiciones son verdaderas el certificado sera valido
        return true;
      } else {
        return false;
      }
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
