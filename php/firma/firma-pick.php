<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql =  $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $id_usr = $row['id_usr'];

    $consulta = "SELECT id_usr, type, nom, ape, apm, dir, tel, cel, mai FROM perf_table WHERE type != 0";
    $sql =  $mysqli->query($consulta);
    if ($sql->num_rows > 0) {
      while ($row = $sql->fetch_assoc()) {
        if ($row['id_usr'] != $id_usr) { //si el usuario es el emisor
          if ($row['type'] == 0) {$type = "nuevo";} elseif ($row['type'] == 1) {$type = "Administrador";} elseif ($row['type'] == 2) {$type = "Usuario";}
          $nombre = $row['nom'] . " " . $row['ape'] . " " . $row['apm'];
          $resultados[] = array("success"=> true, "id"=> $row['id_usr'], "nombre"=> $nombre, "mail"=> $row['mai'], "dir"=>$row['dir'], "tel"=>$row['tel'], "cel"=>$row['cel'], "type"=> $type, "tipo"=> $row['type']);
        }
      }
      $log[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Error de consulta users");
    }

  } else {
    $resultados[] = array("success"=> false, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
