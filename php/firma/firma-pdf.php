<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');
  include_once('../../functions/pdf.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $fechaymd = Date('dmYsiH');
  $localIP = getHostByName(getHostName());

  $resultados = array();
  $log = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $dest = mysqli_real_escape_string($mysqli,$_POST['dest']);
  $doc = mysqli_real_escape_string($mysqli,$_POST['doc']);

  $sql =  $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $id_usr = $row['id_usr'];

    //datos del emisor
    $consulta = "SELECT nom, ape, apm, car, dir, tit FROM perf_table WHERE id_usr = '".$id_usr."'";
    $sql =  $mysqli->query($consulta);
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $enombre = $row['nom'] . " " . $row['ape'] . " " . $row['apm']; //nombre y datos del destinatario
      $ecargo = $row['car'];
      $edireccion = $row['dir'];
      $etitulo = $row['tit'];

    } else {
      $log[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Error de consulta emisor");
    }

    //nombre del archivo json
    $fileTree = '../../data/usr_assets/' . $id_usr . '/' . $id_usr . '_tree.json';
    if (file_exists($fileTree)) {
      $jsonTree = file_get_contents($fileTree);
      $dataTree = json_decode($jsonTree, true);
      foreach ($dataTree as $content) {
        if ($content['id'] == $doc) {
          //revisa extension de archivos
          $filename = '../../data/usr_assets/' . $id_usr . '/' . $content['file'];
          $file_parts = pathinfo($filename);
          if ($file_parts['extension'] == "txt") { //txt
            $fileContent = fopen($filename, "r") or die("Unable to open file!");
            $texto = fread($fileContent,filesize($filename)); //texto del documento
            fclose($fileContent);
          } elseif ($file_parts['extension'] == "docx") { //solo docx
            $texto = read_docx($filename);
          }
          //Leémos los destinatarios
          $consulta = "SELECT nom, ape, apm, car, dir, tit, mai FROM perf_table WHERE id_usr = '".$dest."'";
          $sql =  $mysqli->query($consulta);
          if ($sql->num_rows > 0) {
            $row = $sql->fetch_assoc();
            $nombre = $row['nom'] . " " . $row['ape'] . " " . $row['apm']; //nombre y datos del destinatario
            $cargo = $row['car'];
            $direccion = $row['dir'];
            $titulo = $row['tit'];
            $mail= $row['mai'];
            //mandamos imprimir el documento pdf xml
            $title = $content['name'];
            $pdfreturn = print_pdf($enombre, $ecargo, $edireccion, $etitulo, $nombre, $cargo, $direccion, $titulo, $texto, $id_usr, $dest, $doc);
            //copiamos el archivo a la carpeta del remitente
            $remiteOficio = '../../data/usr_assets/' . $id_usr . '/' . 'oficio_' . $id_usr . '_' . $fechaymd . '_fiej.pdf';
            $pdfcopy = "Proceso completo";
            if (!copy($pdfreturn, $remiteOficio)) {
              $pdfcopy = "No se guardo copia al remitente";
            }
            $resultados[] = array("success"=> true, "message"=> $pdfcopy);
            //resultados meta para el log que leé los documentos por usuario destinatario / destinatario
            $file = 'oficio_' . $id_usr . '_' . $fechaymd . '_fiej.pdf';
            //folio firma enviada
            $idfirma = leeFolio($id_usr);
            $meta[] = array("id"=>  $idfirma . "_fiel", "folio"=> $content['folio'], "file"=> $file, "name"=> $content['name'], "description"=> substr($texto, 0, 250), "content"=> "", "level"=> "output", "destinatario"=> $dest, "date"=> $fechaActual);
            $metd[] = array("id"=>  $idfirma . "_fiel", "folio"=> $content['folio'], "file"=> $file, "name"=> $content['name'], "description"=> substr($texto, 0, 250), "content"=> "", "level"=> "input", "destinatario"=> $dest, "date"=> $fechaActual);

            //una ves creado el log, despues de se enviaron los archivos a sus destinos, creamos la relación en la bdd
            $sql = $mysqli->query("INSERT INTO rela_table (remi, dest, file, fech) VALUES ('".$id_usr."', '".$dest."', '".$file."', '".$fechaActual."')");
            if($sql) {
              $log[] = array("success"=> true, "type"=> "bdd relationship arrangment", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Relacion creada para: " . $id_usr . ", " . $dest . " " . $file);
            } else {
              $log[] = array("success"=> false, "type"=> "bdd relationship arrangment", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Error, no se creo la relacion");
              //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
            }
            $log[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "PDF: data/usr-assets/" . $dest . "/oficio_" . $doc . ".pdf");
          } else {
            $log[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Error de consulta destinatarios");
          }

        }
      }
    }

  } else {
    $resultados[] = array("success"=> false, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }

  //nombre del archivo json y guardado
  $fileTree = '../../data/usr_assets/' . $id_usr . '/' . $id_usr . '_tree.json';
  if (!file_exists($fileTree)) {
    $fileFinal = fopen($fileTree, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileTree, 0777);
  }
  //nombre del archivo json y guardado
  $oldTree = file_get_contents($fileTree);
  $preTree = json_decode($oldTree, true);
  $preTree = array_merge($meta, $preTree);
  //$preTree = $meta;
  $fileFinal = fopen($fileTree, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($preTree, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileTree, 0777);

  //nombre del archivo json y guardado destinatario
  $fileTree = '../../data/usr_assets/' . $dest . '/' . $dest . '_tree.json';
  if (!file_exists($fileTree)) {
    $fileFinal = fopen($fileTree, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileTree, 0777);
  }
  //nombre del archivo json y guardado
  $oldTree = file_get_contents($fileTree);
  $preTree = json_decode($oldTree, true);
  $preTree = array_merge($metd, $preTree);
  //$preTree = $meta;
  $fileFinal = fopen($fileTree, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($preTree, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileTree, 0777);

  //BITACORA
  $fileLog = '../../data/assets/logs/firma.json';
  if (!file_exists($fileLog)) {
    $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileLog, 0777);
  }
  //nombre del archivo json y guardado
  $oldLog = file_get_contents($fileLog);
  $prelog = json_decode($oldLog, true);
  $prelog = array_merge($log, $prelog);
  //$prelog = $resultados;
  $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($prelog, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileLog, 0777);

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
