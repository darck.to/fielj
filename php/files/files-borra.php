<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $doc = mysqli_real_escape_string($mysqli,$_POST['doc']);

  $sql =  $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $id_usr = $row['id_usr'];

    //nombre del archivo json y guardado
    $fileTree = '../../data/usr_assets/' . $id_usr . '/' . $id_usr . '_tree.json';
    $oldTree = file_get_contents($fileTree);
    $preTree = json_decode($oldTree, true);
    //borra el elemento que coincida con el contenido de folio doc
    $i=0;
    foreach($preTree as $content) {
      if ($content['id'] == $doc) {
        $file = '../../data/usr_assets/' . $id_usr . '/' . $content['file'];
        unset($preTree[$i]);
      }
      $i++;
    }
    $preTree = array_values($preTree);

    //reescribimos json
    $fileFinal = fopen($fileTree, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode($preTree, JSON_PRETTY_PRINT));
    fclose($fileFinal);
    //borra el Archivo
    if (unlink($file)) {
      $resultados[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Archivo eliminado " . $file);
    } else {
      $resultados[] = array("success"=> false, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "No se pudo eliminar, contacta soporte");
    }

  } else {
    $resultados[] = array("success"=> false, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }

  //BITACORA
  $fileLog = '../../data/assets/logs/upload.json';
  if (!file_exists($fileLog)) {
    $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileLog, 0777);
  }
  //nombre del archivo json y guardado
  $oldLog = file_get_contents($fileLog);
  $prelog = json_decode($oldLog, true);
  $prelog = array_merge($resultados, $prelog);
  //$prelog = $resultados;
  $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($prelog, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileLog, 0777);

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
