<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  $resultados = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $doc = mysqli_real_escape_string($mysqli,$_POST['doc']);

  $sql =  $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $id_usr = $row['id_usr'];

    //nombre del archivo json y guardado
    $filename = '../../data/usr_assets/' . $id_usr . '/' . $id_usr . '_tree.json';
    if (file_exists($filename)) {

			$file = file_get_contents($filename);
			$json = json_decode($file, true);

			foreach ($json as &$content) {

				if ($content['id'] == $doc) {
          $content['level'] = "filed";
				}

			}

		}
    //abre el archivo y guarda el nuevo arreglo
    $filejson = fopen($filename, 'w') or die ('No file found, contact support');
		if (fwrite($filejson, json_encode($json, JSON_PRETTY_PRINT))) {
			$resultados[] = array("success"=>true ,"message"=> "Archivado");
		} else {
			$resultados[] = array("success"=>false ,"message"=> "No archivo, consulta soporte");
		}
		fclose($filejson);

  } else {
    $resultados[] = array("success"=> false, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }

  //BITACORA
  $fileLog = '../../data/assets/logs/upload.json';
  if (!file_exists($fileLog)) {
    $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileLog, 0777);
  }
  //nombre del archivo json y guardado
  $oldLog = file_get_contents($fileLog);
  $prelog = json_decode($oldLog, true);
  $prelog = array_merge($resultados, $prelog);
  //$prelog = $resultados;
  $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($prelog, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileLog, 0777);

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
