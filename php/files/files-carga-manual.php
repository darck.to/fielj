<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();
  $meta = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $name = mysqli_real_escape_string($mysqli,$_POST['name']);
  $desc = mysqli_real_escape_string($mysqli,$_POST['desc']);
  $cont = mysqli_real_escape_string($mysqli,$_POST['cont']);

  $sql =  $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $id_usr = $row['id_usr']; //id de usuario

    $uploaddir = '../../data/usr_assets/' . $id_usr . '/';
    if (!is_dir($uploaddir)) {
      //CREAMOS LA CARPETA DESTINO
      mkdir($uploaddir, 0755, true);
    }
    //creamos el archivo text
    $uploadfile = $uploaddir . $name . '.txt';
    if (file_exists($uploadfile)) {
      $resultados[] = array("success"=> false, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "No se creo el archivo, archivo duplicado " . $uploadfile);
    } else {
      $myfile = fopen($uploadfile, 'w') or die('error de lectura');
      $txt = $cont;
      fwrite($myfile, $txt);
      fclose($myfile);
      $resultados[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Archivos Subidos en " . $uploadfile);

      //cargamos los archivos y creamos el json que lo va a contener a el, nombre y descripcion
      $folio = leefolio($id_usr);
      $folio_id = substr($id_usr, 0,4) . '_' . $folio; //folioid
      $meta[] = array("id"=> $folio_id, "folio"=> $folio, "file"=> $name . '.txt', "name"=> $name, "description"=> $desc, "content"=> $cont, "level"=> "input", "destinatario"=> "", "date"=> $fechaActual);
      //nombre del archivo json y guardado
      $fileTree = '../../data/usr_assets/' . $id_usr . '/' . $id_usr . '_tree.json';
      if (!file_exists($fileTree)) {
        $fileFinal = fopen($fileTree, 'w') or die ("error de lectura");
        fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
        fclose($fileFinal);
        chmod($fileTree, 0777);
      }
      //nombre del archivo json y guardado
      $oldTree = file_get_contents($fileTree);
      $preTree = json_decode($oldTree, true);
      $preTree = array_merge($meta, $preTree);
      //$preTree = $meta;
      $fileFinal = fopen($fileTree, 'w') or die ("error de lectura");
      fwrite($fileFinal, json_encode($preTree, JSON_PRETTY_PRINT));
      fclose($fileFinal);
      chmod($fileTree, 0777);
    }

  } else {
    $resultados[] = array("success"=> false, "ip"=> $localIP, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }

  //BITACORA
  $fileLog = '../../data/assets/logs/upload.json';
  if (!file_exists($fileLog)) {
    $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
    fwrite($fileFinal, json_encode(array(), JSON_PRETTY_PRINT));
    fclose($fileFinal);
    chmod($fileLog, 0777);
  }
  //nombre del archivo json y guardado
  $oldLog = file_get_contents($fileLog);
  $prelog = json_decode($oldLog, true);
  $prelog = array_merge($resultados, $prelog);
  //$prelog = $resultados;
  $fileFinal = fopen($fileLog, 'w') or die ("error de lectura");
  fwrite($fileFinal, json_encode($prelog, JSON_PRETTY_PRINT));
  fclose($fileFinal);
  chmod($fileLog, 0777);

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
