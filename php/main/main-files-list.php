<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  $resultados = array();

  //"limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql =  $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $id_usr = $row['id_usr'];

    //nombre del archivo json
    $fileTree = '../../data/usr_assets/' . $id_usr . '/' . $id_usr . '_tree.json';
    if (file_exists($fileTree)) {
      $jsonTree = file_get_contents($fileTree);
      $dataTree = json_decode($jsonTree, true);
      foreach ($dataTree as $content) {
        //revisa extension de archivos
        $filename = '../../data/usr_assets/' . $id_usr . '/' . $content['file'];
        $file_parts = pathinfo($filename);
        if ($file_parts['extension'] == "txt") { //txt
          $pdf = false;
        } elseif ($file_parts['extension'] == "docx") { //solo docx
          $pdf = false;
        } elseif ($file_parts['extension'] == "pdf") { //solo pdf
          $pdf = true;
        }
        //fecha
        $fechamx = date("d-m-Y H:i:s", strtotime($content['date']));
        $resultados[] = array('success'=>true, "fir"=> $id_usr, "id"=> $content['id'], "folio"=> $content['folio'], "file"=> $content['file'], "name"=> $content['name'], "description"=> $content['description'], "level"=> $content['level'], "destinatario"=> $content['destinatario'], "date"=> $fechamx, "pdf"=> $pdf);
      }
    } else {
      $resultados[] = array('success'=>true, "message"=> "No hay archivos");
    }

  } else {
    $resultados[] = array("success"=> false, "date"=> $fechaActual, "user"=>$auth, "message"=> "Auth Error");
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
