$(document).ready(function() {

  $(document).on('focus',':input', function(){
    $(this).attr('autocomplete','off');
  })

  //carga files principal (buzon de entrada)
  carga_files()

  function carga_files(e) {
    var folder = localStorage.getItem('aUth_folder');
    if (folder === 'input') {
      var bread = 'entrada';
    } else if (folder === 'output') {
      var bread = 'salida';
    } else if (folder === 'filed') {
      var bread = 'archivo';
    }
    $('#mainFilesBreadcrumb').html(bread);
    var stringaUth_key = localStorage.getItem('aUth_key');
    var stringaUth_user = localStorage.getItem('aUth_user');
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    $.ajax({
      type: 'POST',
      url: 'php/main/main-files-list.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content;
        $('.files-list').html('')
        $.each(data, function (name, value) {
          if (value.success) {
            if (value.level === folder) { //nivel carpeta de entrada
              content = '<li class="list-odd">';
                content += '<i class="far fa-file-alt fa-2x list-icon ma-7-t"></i>';
                content += '<div class="list-box">';
                  content += '<p><b class="text-blue-grey text-u">' + value.file + '</b>&nbsp;|&nbsp;<small class="text-u">' + value.name + '</small></p>';
                  content += '<small>creado ' + value.date + '</small>';
                  content += '<p class="has-text-right">';
                    //si el documento es pdf o de entrada
                    if (value.pdf) {
                      content += '<span class="handed"><a href="data/usr_assets/' + value.fir + '/' + value.file + '" target="_blank"><i class="fas fa-file-pdf font-one-3 has-text-info ma-quar-lr"></i></a></span>';
                    } else {
                      content += '<span class="handed firmaDocumento" value="' + value.id + '"><i class="fas fa-file-signature font-one-3 has-text-info ma-quar-lr"></i></span>';
                    }
                    if (folder != 'filed') { //si la carpeta es la de archivo
                      content += '<span class="handed fileDocumento" value="' + value.id + '"><i class="fas fa-archive font-one-3 has-text-grey ma-quar-lr"></i></span>';
                      content += '<span class="handed borraDocumento" value="' + value.id + '"><i class="far fa-trash-alt font-one-3 has-text-danger ma-quar-lr"></i></span>';
                    }
                  content += '</p>';
                content += '</div>';
              content += '</li>';
              $('.files-list').append(content);
            }
            //PAJINATOR
    		  	$(".files-list").pagify(6, ".list-odd");
          } else {
            toast(value.message)
          }
        })

        //firma documento
        $('.firmaDocumento').on('click', function(e){
          var doc = $(this).attr('value');
          localStorage.setItem("aUth_doc",doc);
          modal();
          $('.modal-content').load('templates/firma/firma-form.html')
        })

        //archiva documento
        $('.fileDocumento').on('click', function(e){
          var doc = $(this).attr('value');
          localStorage.setItem("aUth_doc",doc);
          modal();
          content = '<div class="box has-background-white">';
            content += '<h1><b class="has-text-grey text-u">Archivado de Documento</b></h1>';
            content += '<hr/>';
            content += '<div class="section has-text-right">';
              content += '<button class="button is-medium is-warning ma-half-lr archivaFile">Archivar</button></h4>';
            content += '</div>';
          content += '<div>';
          $('.modal-content').html(content);

          $('.archivaFile').on('click', function(e) {
            var stringaUth_key = localStorage.getItem('aUth_key');
            var stringaUth_user = localStorage.getItem('aUth_user');
            var doc = localStorage.getItem('aUth_doc');
            var form_data = new FormData();
            form_data.append('auth',stringaUth_key);
            form_data.append('user', stringaUth_user);
            form_data.append('doc', doc);
            $.ajax({
              type: 'POST',
              url: 'php/files/files-archiva.php',
              data: form_data,
              cache: false,
              contentType: false,
              processData: false,
              async: true,
              success: function(data) {
                $.each(data, function (name, value) {
                  if (value.success) {
                    toast(value.message);
                    xmodal();
                    carga_main_files(localStorage.getItem('aUth_folder'))
                  } else {
                    toast(value.message)
                  }
                })
              },
              error: function(xhr, tst, err) {
                toast(err)
              }
            })
          })
        })

        //borra documento
        $('.borraDocumento').on('click', function(e){
          var doc = $(this).attr('value');
          localStorage.setItem("aUth_doc",doc);
          modal();
          content = '<div class="box has-background-white">';
            content += '<h1><b class="has-text-grey text-u">Borrado de Documento</b></h1>';
            content += '<hr/>';
            content += '<div class="section has-text-right">';
              content += '<button class="button is-medium is-danger ma-half-lr borrarFile">Borrar</button></h4>';
            content += '</div>';
          content += '<div>';
          $('.modal-content').html(content);

          $('.borrarFile').on('click', function(e) {
            var stringaUth_key = localStorage.getItem('aUth_key');
            var stringaUth_user = localStorage.getItem('aUth_user');
            var doc = localStorage.getItem('aUth_doc');
            var form_data = new FormData();
            form_data.append('auth',stringaUth_key);
            form_data.append('user', stringaUth_user);
            form_data.append('doc', doc);
            $.ajax({
              type: 'POST',
              url: 'php/files/files-borra.php',
              data: form_data,
              cache: false,
              contentType: false,
              processData: false,
              async: true,
              success: function(data) {
                $.each(data, function (name, value) {
                  if (value.success) {
                    toast(value.message);
                    xmodal();
                    carga_main_files(localStorage.getItem('aUth_folder'))
                  } else {
                    toast(value.message)
                  }
                })
              },
              error: function(xhr, tst, err) {
                toast(err)
              }
            })
          })
        })

      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

});
