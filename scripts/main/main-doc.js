$(document).ready(function() {

  $(document).on('focus',':input', function(){
    $(this).attr('autocomplete','off');
  })

  //carga files principal (buzon de entrada)
  carga_doc()

  function carga_doc(e) {
    var bread = 'documentaci&oacute;n';
    $('#mainFilesBreadcrumb').html(bread);
    var stringaUth_key = localStorage.getItem('aUth_key');
    var stringaUth_user = localStorage.getItem('aUth_user');
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    $.ajax({
      type: 'POST',
      url: 'php/main/main-files-list.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content;
        $('.files-list').html('')
        $.each(data, function (name, value) {
          if (value.success) {
            if (value.level === folder) { //nivel carpeta de entrada
              content = '<li class="list-odd">';
                content += '<i class="far fa-file-alt fa-2x list-icon ma-7-t"></i>';
                content += '<div class="list-box">';
                  content += '<p><b class="text-blue-grey text-u">' + value.file + '</b>&nbsp;|&nbsp;<small class="text-u">' + value.name + '</small></p>';
                  content += '<small>creado ' + value.date + '</small>';
                  content += '<p class="has-text-right">';
                    //si el documento es pdf o de entrada
                    if (value.pdf) {
                      content += '<span class="handed"><a href="data/usr_assets/' + value.fir + '/' + value.file + '" target="_blank"><i class="fas fa-file-pdf font-one-3 has-text-info ma-quar-lr"></i></a></span>';
                    } else {
                      content += '<span class="handed firmaDocumento" value="' + value.id + '"><i class="fas fa-file-signature font-one-3 has-text-info ma-quar-lr"></i></span>';
                    }
                    if (folder != 'filed') { //si la carpeta es la de archivo
                      content += '<span class="handed fileDocumento" value="' + value.id + '"><i class="fas fa-archive font-one-3 has-text-grey ma-quar-lr"></i></span>';
                      content += '<span class="handed borraDocumento" value="' + value.id + '"><i class="far fa-trash-alt font-one-3 has-text-danger ma-quar-lr"></i></span>';
                    }
                  content += '</p>';
                content += '</div>';
              content += '</li>';
              $('.files-list').append(content);
            }
            //PAJINATOR
    		  	$(".files-list").pagify(6, ".list-odd");
          } else {
            toast(value.message)
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

});
