$(document).ready(function() {

  $(document).on('focus',':input', function(){
    $(this).attr('autocomplete','off');
  })

  //carga archivos mediante drop o file
  $('.subirArchivos').on('click', function(e) {
    modap();
    $('.modal-content').load('templates/files/files-carga.html')
  })

  //carga files principal (buzon de entrada)
  carga_main_files('input')
  $('.refresh-nav').attr('val', 'input');

  //navegador de menu lateral
  $('.nav-files').on('click', function(e) {
    var folder = $(this).attr('val');
    carga_main_files(folder)
  })

});
