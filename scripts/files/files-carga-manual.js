$(document).ready(function() {

  $(document).on('focus',':input', function() {
    $(this).attr('autocomplete','off')
  })

  $('.cargaDocumentoM').on('click', function(e) {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var name = $('#i_name').val();
    var desc = $('#i_desc').val();
    var cont = $('#i_cont').val();
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('name', name);
    form_data.append('desc', desc);
    form_data.append('cont', cont);
    $.ajax({
      type: 'POST',
      url: 'php/files/files-carga-manual.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            toast(value.message);
            xmodal();
            carga_main_files(localStorage.getItem('aUth_folder'))
          } else {
            toast(value.message)
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  })

});
