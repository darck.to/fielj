$(document).ready(function() {

  $(document).on('focus',':input', function() {
    $(this).attr('autocomplete','off')
  })

  //carga archivos mediante file
  $("#i_file").change(function() {
    $('.files-box-actions').html('<hr/>');//se limpia el contenedor y se le agregara un boton para confirmar la carga de archivos
    var numFiles = $(this)[0].files.length
    if (numFiles > 6) { //No mas de 6 archivos por carga
      var txt = '<div class="card has-background-danger pa-half four-w display-inline-b">';
        txt += '<p class="has-text-light ma-b-no">Intentaste subir ' + numFiles + ' archivos</p>';
        txt += '<small class="has-text-light">No puedes subir m&aacute;s de 6 a la ves</small>';
      txt += '</div>';
      $('.files-box-actions').append(txt)
      return false
    }
    //despues de contar los archivos seleccionados, creamos sus inputs > agregamos nombres > y tamaño de cada uno
    for (var i = 0; i < numFiles; i++) {
      var fSize = ($(this)[0].files[i].size / 1024);
      if (fSize > 20480) { //Si es mayor de 2 megas no lo subira
        var txt = '<div class="card has-background-warning pa-half four-w display-inline-b">';
          txt += '<p class="text-blue-grey ma-b-no f-name-' + i + '"></p>';
          txt += '<small>Archivos no pueden medir mas de 2 mb</small>';
        txt += '</div>';
        $('.files-box-actions').append(txt)
        $('.f-name-' + i).html($(this)[0].files[i].name)
      } else {
        var txt = '<div class="card file-list-' + i + ' pa-half four-w display-inline-b list-odd">';
          txt += '<p class="text-blue-grey ma-b-no f-name-' + i + '"></p>';
          txt += '<small class="f-size-' + i + '"></small>';
          txt += '<hr />';
          txt += '<div class="field">';
            txt += '<div class="control">';
              txt += '<input id="i_name_' + i +  '" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Nombre">';
              txt += '<input id="i_desc_' + i +  '" class="input is-small placeholder-b-b ma-quat-b" type="text" placeholder="Descripci&oacute;n">';
            txt += '</div>';
          txt += '</div>';
        txt += '</div>';
        $('.files-box-actions').append(txt)
        $('.f-name-' + i).html($(this)[0].files[i].name)
        if (fSize / 1024 > 1) {
          if (((fSize / 1024) / 1024) > 1) {
            fSize = (Math.round(((fSize / 1024) / 1024) * 100) / 100);
            $('.f-size-' + i).html( fSize + "Gb")
          } else {
            fSize = (Math.round((fSize / 1024) * 100) / 100)
            $('.f-size-' + i).html( fSize + "Mb")
          }
        } else {
          fSize = (Math.round(fSize * 100) / 100)
          $('.f-size-' + i).html( fSize  + "kb")
        }
      }
    }
    $('.files-box-actions').append('<hr/><button class="button is-info is-pulled-right cargaDocumento">Cargar Archivos</button>')
    //carga documentos
    $('.cargaDocumento').on('click', function(e) {
      var stringaUth_key = localStorage.getItem("aUth_key");
      var stringaUth_user = localStorage.getItem("aUth_user");
      var myfiles = document.getElementById('i_file');
      var files = myfiles.files;
      var form_data = new FormData();
      form_data.append('auth',stringaUth_key);
      form_data.append('user', stringaUth_user);
      for (i = 0; i < files.length; i++) {
        form_data.append('file' + i, files[i]);
        form_data.append('name' + i, $('#i_name_' + i).val());
        form_data.append('desc' + i, $('#i_desc_' + i).val())
      }
      $.ajax({
        type: 'POST',
        url: 'php/files/files-carga.php',
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        async: true,
        success: function(data) {
          $.each(data, function (name, value) {
            if (value.success) {
              toast(value.message);
              carga_main_files(localStorage.getItem('aUth_folder'));
              xmodal()
            } else {
              toast(value.message)
            }
          })
        },
        error: function(xhr, tst, err) {
          toast(err)
        }
      })
    })

  })

  //carga manual
  $('.cargaManual').on('click', function(e) {
    modap();
    $('.modal-content').load('templates/files/files-carga-manual.html')
  })


});
