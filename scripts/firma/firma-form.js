$(document).ready(function() {

  $(document).on('focus',':input', function(){
    $(this).attr('autocomplete','off');
  })

  //carga files form (buzon de entrada)
  carga_files_form()

  function carga_files_form(e) {
    $('#mainFilesBreadcrumb').html('entrada');
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var doc = localStorage.getItem("aUth_doc");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('doc', doc);
    $.ajax({
      type: 'POST',
      url: 'php/files/files-carga-doc.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content;
        $('#cargaFirmaForm').html('')
        $.each(data, function (name, value) {
          if (value.success) {
            content = '<h1><b class="has-text-grey text-u">Folio:&nbsp;' + value.folio + '</b>&nbsp;<b class="text-blue-grey">' + value.file + '</b></h1>';
            content += '<hr/>';
            content += '<small class="has-text-grey caret-right">Nombre del Oficio</small>';
            content += '<input type="text" class="input ma-half-ud inp-dis nameInput" value="' + value.name + '"/>';
            content += '<small class="has-text-grey">Descripci&oacute;n</small>';
            content += '<input type="text" class="input ma-half-ud inp-dis descInput" value="' + value.description + '"/>';
            content += '<hr/>';
            content += '<textarea class="textarea inp-dis textInput">' + value.content + '</textarea>';
            content += '<hr/>';
            content += '<h4 class="has-text-right"><button class="button is-medium is-primary ma-half-lr inp-dis guardaMeta">Guardar</button>';
            content += '<button class="button is-medium is-info ma-half-lr inp-dis firmarFile">Firmar</button></h4>';
            content += '<hr/>';
            content += '<div id="firmaPick"></div>'
            content += '<h4 class="has-text-right"><small cloas><b class="has-text-grey">Creado:</b>&nbsp;' + value.date + '</small></h4>';
            $('#cargaFirmaForm').append(content);
          } else {
            toast(value.message)
          }
        })

        //guarda datos meta
        $('.guardaMeta').on('click', function(e){
          guardaMeta()
        })

        //firma documento
        $('.firmarFile').on('click', function(e){
          firmaDocumento()
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  //guarda metadatos
  function guardaMeta() {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var doc = localStorage.getItem("aUth_doc");
    var name = $('.nameInput').val();
    var desc = $('.descInput').val();
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('doc', doc);
    form_data.append('name', name);
    form_data.append('desc', desc);
    $.ajax({
      type: 'POST',
      url: 'php/files/files-edita-meta.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            toast(value.message);
            xmodal();
            carga_main_files(localStorage.getItem('aUth_folder'))
          } else {
            toast(value.message)
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  //firma documentos
  function firmaDocumento() {
    $('.inp-dis').prop('disabled', true);
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    $.ajax({
      type: 'POST',
      url: 'php/firma/firma-pick.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content;
        $('#firmaPick').html('')
        content = '<small class="has-text-grey caret-right">Selecciona un Destinatario</small>';
        content += '<div class="columns">';
          content += '<div class="column is-6">';
            content += '<div class="select is-multiple ma-half-ud">';
              content += '<select size="4" class="auto-h pa-no inp-dis">';
              $.each(data, function (name, value) { //agregamos los usuarios en option con su id y nombre
                if (value.success) {
                  content += '<option value="' + value.id + '" mai="' + value.mail + '" nom="' + value.nombre + '" dir="' + value.dir + '" tel="' + value.tel + '" cel="' + value.cel + '">' + value.dir + '&nbsp;|&nbsp;' + value.nombre + '</option>';
                } else {
                  toast(value.message)
                }
              })
              content += '</select>';
            content += '</div>';
          content += '</div>';
          content += '<div class="column is-6">';
            content += '<div id="userTags" class="tags"></div>';
          content += '</div>';
        content += '</div>';
        content += '<hr/>';
        content += '<div class="columns">';
          content += '<div class="column is-12">';
            content += '<h4 class="has-text-right"><button class="button is-medium is-info ma-half-lr inp-dis sendFile">Certificar</button></h4>';
          content += '</div>';
        content += '</div>';
        content += '<hr/>';
        content += '<div id="firmaCert"></div>';
        $('#firmaPick').html(content)

        //pick a los usuarios seleccionados
        $('select').on('change', function() {
          var nom = $('option:selected', this).attr('nom') //name
          var mai = $('option:selected', this).attr('mai') //mail
          var dir = $('option:selected', this).attr('dir') //dir
          var tel = $('option:selected', this).attr('tel') //tel
          var cel = $('option:selected', this).attr('cel') //cel
          $(this).fadeOut('fast', function() {
            $(this).addClass('is-hidden'); //esconde el option
          });
          $('.tag span').each(function(){
            if ($(this).html() === mai) {
              $(this).parent().fadeOut('fast', function() {
                $(this).remove()
              }) //elimina el parent si se duplica un tag
            }
          })

          $('#userTags').append('<span class="tag is-primary tag-dest" val="'+ this.value +'"><span class="is-relative">' + nom + '<span class="box tag-desc"><i>' + dir + '</i><i>' + mai +'</i><i>' + tel +'</i><i>' + cel +'</i></span></span><button class="delete inp-dis is-small delTag"></button></span>');
          //borrar tag
          $('.delTag').on('click', function(e) {
            $(this).parent().fadeOut("normal", function() {
              $("option:selected").prop("selected", false);
              $('select').fadeIn('fast', function() {
                $('select').removeClass('is-hidden'); //muestra el option
              });
              $(this).remove()
            })
          })
        })
        //enviar archivo
        $('.sendFile').on('click', function(e) {
          $('.inp-dis').prop('disabled', true);
          //cuenta los destinatarios
          toast('N&uacute;mero de destinatarios: ' + $('.tag-dest').length);
          var num = $('.tag span').length;
          //agregamos el file input
          content = '<small class="has-text-grey caret-right">Carga tu certificado</small>';
          content += '<div class="columns">';
            content += '<div class="column is-12">';
              content += '<div class="content files-box pa-half-t">';
                content += '<div class="file is-boxed ma-ce-a file-color">';
                  content += '<label class="file-label ma-ce-a">';
                    content += '<input id="i_cert" class="file-input" type="file" name="certfile" accept=".zip"/>';
                    content += '<span class="file-cta">';
                      content += '<span class="file-icon">';
                        content += '<i class="fas fa-upload"></i>';
                      content += '</span>';
                      content += '<span class="file-label">';
                        content += 'Selecciona un Archivo';
                      content += '</span>';
                    content += '</span>';
                  content += '</label>';
                content += '</div>';
              content += '</div>';
            content += '</div>';
          content += '</div>';
          content += '<hr/>';
          $('#firmaCert').html(content)
          //revisamos la validez del archivo
          $("#i_cert").change(function() {
            firma_check();
          })
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  //check certificado valido
  function firma_check() {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var file_data = $('#i_cert').prop('files')[0];
    var form_data = new FormData();
    form_data.append('auth', stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('file', file_data);
    $.ajax({
      type: 'POST',
      url: 'php/firma/firma-check.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            $('.file-color').removeClass('is-danger');
            $('.file-color').addClass('is-primary');
            $('.file-icon').html('<i class="far fa-check-circle"></i>');
            //$('#i_cert').prop('disabled', true);
            crear_pdf(); //creamos el pdf y lo mandamos a descargar
            toast(value.message)
          } else {
            $('.file-color').removeClass('is-primary');
            $('.file-color').addClass('is-danger');
            $('.file-icon').html('<i class="far fa-times-circle"></i>');
            toast(value.message)
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  //creamos el documento certificado
  function crear_pdf(e) {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var doc = localStorage.getItem("aUth_doc");
    var form_data = new FormData();
    form_data.append('auth', stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('doc', doc);
    form_data.append('dest', $('.tag-dest').attr('val'));
    $.ajax({
      type: 'POST',
      url: 'php/firma/firma-pdf.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: false,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            toast(value.message);
            carga_main_files(localStorage.getItem('aUth_folder'));
            xmodal()
          } else {
            toast(value.message)
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

});
