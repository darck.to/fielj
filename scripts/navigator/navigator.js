$(document).ready(function() {
  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {
    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  })

  check_level();

  function check_level(e) {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    $.ajax({
      type: 'POST',
      url: 'php/init/init-level.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content = "";
        $('.navbar-adm').html('')
        $.each(data, function (name, value) {
          if (value.type) {
            content += '<a class=\"navbar-link\">Configuraci&oacute;n</a>';
            content += '<div class=\"navbar-dropdown\">';
              $.each(value.menu, function(id, key) {
                content += '<a class=\"navbar-item go-nav" value="' + key.url + '">' + key.level + '</a>';
              })
            content += '</div>';
            $('.navbar-adm').addClass('navbar-item has-dropdown is-hoverable');
            $('.navbar-adm').html(content);
            toast(value.message)
          } else {
            toast(value.message)
          }
        })
        //gonavigator
        $('.go-nav').on('click', function(e) {
          var nav = $(this).attr('value');
          navigation(nav)
        })
      },
      error: function(xhr, tst, err) {
        toast(err);
      }
    })
  }

})

//SESSION CLOSE
$('#session-close').on('click', function(e){
  //CIERRA SESION PHP
  $.ajax({
    type: 'POST',
    url: 'php/init/init-start.php',
    data: {
      a: 0
    },
    cache: false,
    success: function(data) {
      toast('Session Cerrada');
      localStorage.removeItem("aUth_key");
      localStorage.removeItem("aUth_user");
      setTimeout(location.reload.bind(location), 1000);
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
})
